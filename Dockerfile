FROM node:10

RUN mkdir -p /usr/app

WORKDIR /usr/app

RUN npm install

EXPOSE 8080

CMD ["npm", "run", "watch"]