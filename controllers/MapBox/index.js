const mbxGeocoding = require("@mapbox/mapbox-sdk/services/geocoding");
require("dotenv").config({ path: "../../.env" });
const geocodingClient = mbxGeocoding({
  accessToken: process.env.mapBoxToken
});

module.exports = async city => {
  try {
    let geocoding = await geocodingClient
      .forwardGeocode({
        query: city,
        limit: 2
      })
      .send();
    const { coordinates } = geocoding.body.features[0].geometry;

    return coordinates;
  } catch (error) {
    throw Error(error);
  }
};
