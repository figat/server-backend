const Post = require("../../models/Post.js");
const User = require("../../models/User");

const index = async (...params) => {
  try {
    return await Post.find({});
  } catch (error) {
    throw Error(error);
  }
};

const show = async id => {
  try {
    return await Post.findOne({ _id: id });
  } catch (error) {
    throw Error(error);
  }
};

const create = async (post = new Post()) => {
  if (!(post instanceof Post)) return Error("Post object required");
  try {
    //push post's id to user document
    return await Post.create(post);
  } catch (error) {
    throw Error(error);
  }
};

const update = async payload => {
  const { id, content } = payload;
  try {
    return await Post.updateOne({ _id: id }, { content });
  } catch (error) {
    throw Error(error);
  }
};

const destroy = async id => {
  try {
    await User.update({ posts: id }, { $pull: { posts: id } });
    return await Post.deleteOne({ _id: id });
  } catch (error) {
    throw Error(error);
  }
};

module.exports = {
  index,
  show,
  create,
  update,
  destroy
};
