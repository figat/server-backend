const fetch = require("node-fetch");
require("dotenv").config({ path: "../../.env" });

module.exports = async city => {
  try {
    const response = await fetch(
      `https://en.wikipedia.org/w/api.php?action=opensearch&search=${city}&format=json`
    );

    if (!response.ok) {
      return res.status(400).json({ message: "Fetch error" });
    }

    return { wikiInfo: response.json() };
  } catch (error) {
    throw Error(error);
  }
};
