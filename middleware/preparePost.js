const Post = require("../models/Post");
const foursquare = require("../controllers/FoursquareController/index");
const coordinates = require("../controllers/MapBox/index");
const events = require("../controllers/PredictController");

module.exports = async (req, res, next) => {
  const { city } = req.body.city;
  if (city === "") {
    return res.status(404).json({ error: "Not Found" });
  }
  req.post = new Post({
    city,
    createdAt: new Date().getTime(),
    foursquare: await foursquare(city),
    location: await coordinates(city),
    events: await events(city),
    author: req.user.data
  });

  await next();
};
