const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const salt = 10;
const { check, validationResult } = require("express-validator");

module.exports = async (req, res, next) => {
  const { username, email, password } = req.body;

  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }
  if (!password) {
    res.status(400).json({ error: "Something went wrong" });
  } else {
    const hash = await bcrypt.hash(password, salt);
    req.user = new User({
      userName: username,
      userEmail: email,
      userPassword: hash,
      authToken: jwt.sign(
        {
          iat: new Date().getTime()
        },
        "secret",
        {
          expiresIn: "2d"
        }
      )
    });
    await next();
  }
};
