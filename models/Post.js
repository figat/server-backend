// db.places.aggregate( [
//   {
//      $geoNear: {
//         near: { type: "Point", coordinates: [ -73.9667, 40.78 ] },
//         spherical: true,
//         query: { category: "Parks" },
//         distanceField: "calcDistance"
//      }
//   }
// ] )

// db.places.find(
//   {
//     location:
//       { $near:
//          {
//            $geometry: { type: "Point",  coordinates: [ -73.9667, 40.78 ] },
//            $minDistance: 1000,
//            $maxDistance: 5000
//          }
//       }
//   }
// )

const mongoose = require("mongoose");

const Post = new mongoose.Schema({
  createdAt: { type: Date, required: true },
  foursquare: [],
  location: [],
  events: [],
  city: { type: String },
  author: { type: mongoose.Schema.Types.ObjectId, ref: "User" }
  // relation Post belongs_to User
});

module.exports = mongoose.model("Post", Post);
