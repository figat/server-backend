const express = require("express");
const router = express.Router();
const PostsController = require("../../controllers/PostsController");
const preparePost = require("../../middleware/preparePost");
const User = require("../../models/User");
const Post = require("../../models/Post");
const { protectedRoutes } = require("../../middleware/ProtectedRoutes");

router.get("/api/details/:id", protectedRoutes, async (req, res) => {
  try {
    const { id } = req.params;
    const details = await PostsController.show({ _id: id });
    res.json({ details });
  } catch (err) {
    console.log(err);
  }
});

router.get("/api/posts", protectedRoutes, async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.user.data });
    res.json(await Post.find({ author: user }));
  } catch (err) {
    console.log(err);
  }
});
router.post("/api/post", protectedRoutes, preparePost, async (req, res) => {
  const { events } = req.post;
  const { foursquare } = req.post;

  const isArrayCheck = check => {
    return !Array.isArray(check) || !check.length;
  };

  try {
    if (isArrayCheck(events) && isArrayCheck(foursquare)) {
      return res.status(404).json({ error: "Not Found" });
    }

    User.updateOne(
      { _id: req.user.data },
      { $push: { posts: req.post._id } },
      (error, success) => {
        if (error) {
          console.log(error);
        } else {
          console.log("success: User.update===> push: { posts: req.post._id }");
        }
      }
    );
    res.json(await PostsController.create(req.post));
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Server error" });
  }
});

module.exports = router;
