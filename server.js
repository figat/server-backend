const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");

const db = require("./config/db");
const server = express();
const Port = process.env.PORT || 8080;
const User = require("./models/User");

//function that searches all database
async function all() {
  let user = await User.find({});
  console.log(user);
}
all();

server.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000"
  })
);

const { userRouter, postsRouter } = require("./routes/api");

//Middleware
server.use(cookieParser());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(morgan("tiny"));

//DB connect
db.dbConnection();

// Use Routes
server.use("/api/user", userRouter);
server.use(postsRouter);
server.listen(Port, () => {
  console.log(`Server running on port:${Port}`);
});
